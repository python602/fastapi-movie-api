# Curso de FastAPI: Introducción, Operaciones, Validaciones y Autenticación

Curso [platzi](https://platzi.com/cursos/fastapi/)

## Install

Como ejecutar

```sh
git checkout main
python3.10 -m venv env
pip install -r requirements.txt
uvicorn main:app --reload
```

# Curso de FastAPI: Base de Datos, Modularización y Deploy a Producción

Curso [platzi](https://platzi.com/cursos/fastapi-modularizacion-datos/)
## Install

Como ejecutar

```sh
git checkout modularizacion
python3.10 -m venv env
pip install -r requirements.txt
uvicorn main:app --reload
```